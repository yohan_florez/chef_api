<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'cors', 'prefix' => 'v1'], function () {

	Route::post('login', 'AuthController@authenticate');

	Route::group(['middleware' => ['before' => 'jwt.auth']], function () {

		Route::resource('hero', 'HeroController');
	});
});